// JavaScript Document

$oldMenu = 'void';
$newMenu = 'void';
$isProjectsMenuOpen = false;
$isDesignsMenuOpen = false;
$isSketchesMenuOpen = false;

$projects = []; // labels projects
$designs = []; // labels designs
$sketches = []; // labels sketches

$(document).ready(function () {
	
	xmlParser();
	
	onResizeMenu();

	// assign on resize action
	$(window).resize(onResize);

}); //end function

function xmlParser() {
	
	for(i = 0; i < 5; i++){
		$projects.push('project #' + parseInt(i+1));
		$designs.push('design #' + parseInt(i+1));
		$sketches.push('sketches #' + parseInt(i+1));
	}
	
	$menuHeight = 38 + 25 + 15 + 25 + 13*5 + 8*2 + 15;
	$menuHeightOnProjects = $menuHeight + 15 + $projects.length*12 +15;
	$menuHeightOnDesigns = $menuHeight + 15 + $designs.length*12 +15;
	$menuHeightOnSketches = $menuHeight + 15 + $sketches.length*12 +15;
	
	$('#projectsMenu').click(function(){
	
		$('nav ul li').css("opacity",0.55);
		$(this).css("opacity",1);
		
		$newMenu = 'PROJECTS';
		
		if(!$isProjectsMenuOpen && !$isSketchesMenuOpen && !$isDesignsMenuOpen){
			openProjectsMenu('void');
		}
		else{
			if($isSketchesMenuOpen){
				closeSketchesMenu('projects');
			}
			else{
				if($isDesignsMenuOpen){
					closeDesignsMenu('projects');
				}
			}
		}		
	
	});
	
	$('#designsMenu').click(function(){
	
		$('nav ul li').css("opacity",0.55);
		$(this).css("opacity",1);
		
		$newMenu = 'DESIGN';
		
		if(!$isDesignsMenuOpen && !$isProjectsMenuOpen && !$isSketchesMenuOpen){
			openDesignsMenu('void');
		}
		else{
			if($isSketchesMenuOpen){
				closeSketchesMenu('designs');
			}
			else{
				if($isProjectsMenuOpen){
					closeProjectsMenu('designs');
				}
			}
		}
		
	});
	
	$('#sketchesMenu').click(function(){
	
		$('nav ul li').css("opacity",0.55);
		$(this).css("opacity",1);
		
		$newMenu = 'SKETCHES';
		
		if(!$isSketchesMenuOpen && !$isProjectsMenuOpen && !$isDesignsMenuOpen){
			openSketchesMenu('void');
		}
		else{
			if($isDesignsMenuOpen){
				closeDesignsMenu('sketches');
			}
			else{
				if($isProjectsMenuOpen){
					closeProjectsMenu('sketches');
				}
			}
		}
		
	});
	
	$('#bioMenu').click(function(){
	
		$('nav ul li').css("opacity",0.55);
		$(this).css("opacity",1);
		
		$newMenu = 'BIO';
		
		if($isProjectsMenuOpen) closeProjectsMenu('void');
		if($isDesignsMenuOpen) closeDesignsMenu('void');
		if($isSketchesMenuOpen) closeSketchesMenu('void');
		
	});
	
	$('#contactMenu').click(function(){
	
		$('nav ul li').css("opacity",0.55);
		$(this).css("opacity",1);
		
		$newMenu = 'CONTACT';
		
		if($isProjectsMenuOpen) closeProjectsMenu('void');
		if($isDesignsMenuOpen) closeDesignsMenu('void');
		if($isSketchesMenuOpen) closeSketchesMenu('void');
		
	});

} // end function

function onResizeMenu() {
	
	if(!$isDesignsMenuOpen && !$isProjectsMenuOpen && !$isSketchesMenuOpen){
		$('#menu').css("margin-top",($(window).height() - $menuHeight)/2);
	}
	else{
		if($isProjectsMenuOpen){
			$('#menu').css("margin-top",($(window).height() - $menuHeightOnProjects)/2);
		}
		else{
			if($isDesignsMenuOpen){
				$('#menu').css("margin-top",($(window).height() - $menuHeightOnDesigns)/2);
			}
			else{
				$('#menu').css("margin-top",($(window).height() - $menuHeightOnSketches)/2);
			}
		}
	}
	
} // end function

function onResize() {
	
	onResizeMenu();
	
} // end function

function openProjectsMenu(previousMenu){

	$isProjectsMenuOpen = true;
	
	$subMenu='';
	for(i = 0; i < $projects.length; i++){
		$subMenu += '<li class="subMenu">' + $projects[i] + '</li>';
	}

	if(previousMenu == 'void'){
		TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:$menuHeightOnProjects - $menuHeight, ease:Power2.easeInOut});

		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnProjects)/2, ease:Power2.easeInOut, 
			onComplete:function(){
				$('#projectsMenu').after($subMenu);
				$('#projectsMenu').css("opacity",1);
				$('#projectsMenu').css("margin-bottom",15);
				$('#designsMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
				}
		});
	}
	if(previousMenu == 'designs'){
		TweenLite.to($('#designsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
		TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:$menuHeightOnProjects - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnProjects)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#projectsMenu').after($subMenu);
				$('#projectsMenu').css("opacity",1);
				$('#projectsMenu').css("margin-bottom",15);
				$('#designsMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	if(previousMenu == 'sketches'){
		TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:15, ease:Power2.easeInOut});
		TweenLite.to($('#bioMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:$menuHeightOnProjects - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnProjects)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#projectsMenu').after($subMenu);
				$('#projectsMenu').css("opacity",1);
				$('#projectsMenu').css("margin-bottom",15);
				$('#designsMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	
} // end function

function openDesignsMenu(previousMenu){

	$isDesignsMenuOpen = true;
	
	$subMenu='';
	for(i = 0; i < $designs.length; i++){
		$subMenu += '<li class="subMenu">' + $designs[i] + '</li>';
	}

	if(previousMenu == 'void'){
		TweenLite.to($('#designsMenu'), 0.5, {marginBottom:$menuHeightOnDesigns - $menuHeight, ease:Power2.easeInOut});

		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnDesigns)/2, ease:Power2.easeInOut, 
			onComplete:function(){
				$('#designsMenu').after($subMenu);
				$('#designsMenu').css("opacity",1);
				$('#designsMenu').css("margin-bottom",15);
				$('#sketchesMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	if(previousMenu == 'projects'){
		TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
		TweenLite.to($('#designsMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#designsMenu'), 0.5, {marginBottom:$menuHeightOnDesigns - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnDesigns)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#designsMenu').after($subMenu);
				$('#designsMenu').css("opacity",1);
				$('#designsMenu').css("margin-bottom",15);
				$('#sketchesMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	if(previousMenu =='sketches'){
		TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:15, ease:Power2.easeInOut});
		TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#designsMenu'), 0.5, {marginBottom:$menuHeightOnSketches - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnSketches)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#designsMenu').after($subMenu);
				$('#designsMenu').css("opacity",1);
				$('#designsMenu').css("margin-bottom",15);
				$('#sketchesMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	

} // end function

function openSketchesMenu(previousMenu){

	$isSketchesMenuOpen = true;
	
	$subMenu='';
	for(i = 0; i < $sketches.length; i++){
		$subMenu += '<li class="subMenu">' + $sketches[i] + '</li>';
	}

	if(previousMenu == 'void'){	
		TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:$menuHeightOnSketches - $menuHeight, ease:Power2.easeInOut});

		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnSketches)/2, ease:Power2.easeInOut, 
			onComplete:function(){
				$('#sketchesMenu').after($subMenu);
				$('#sketchesMenu').css("opacity",1);
				$('#sketchesMenu').css("margin-bottom",15);
				$('#bioMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	if(previousMenu == 'projects'){
		TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
		TweenLite.to($('#designsMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:$menuHeightOnSketches - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnSketches)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#sketchesMenu').after($subMenu);
				$('#sketchesMenu').css("opacity",1);
				$('#sketchesMenu').css("margin-bottom",15);
				$('#bioMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}
	if(previousMenu =='designs'){
		TweenLite.to($('#designsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
		TweenLite.to($('#bioMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
		TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:$menuHeightOnSketches - $menuHeight, ease:Power2.easeInOut});
		TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeightOnSketches)/2, ease: Power2.easeInOut,
			onComplete:function(){
				$('#sketchesMenu').after($subMenu);
				$('#sketchesMenu').css("opacity",1);
				$('#sketchesMenu').css("margin-bottom",15);
				$('#bioMenu').css("margin-top",15);
				TweenLite.to($('.subMenu'), 0.5, {opacity:0.7, ease:Power2.easeInOut});
			}
		});
	}

} // end function

function closeProjectsMenu(nextMenu){
	
	$isProjectsMenuOpen = false;
	
	TweenLite.to($('.subMenu'), 0.5, {opacity:0, ease:Power2.easeInOut,
		onComplete:function(){
			$('#projectsMenu').css("margin-bottom",$menuHeightOnProjects - $menuHeight);
			$('.subMenu').remove();
			if(nextMenu == 'void'){
				TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeight)/2, ease:Power2.easeInOut});
				TweenLite.to($('#projectsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
				TweenLite.to($('#designsMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#bioMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
			}
			if(nextMenu == 'designs') openDesignsMenu('projects');
			if(nextMenu == 'sketches') openSketchesMenu('projects');
		}
	});
	
} // end function

function closeDesignsMenu(nextMenu){
	
	$isDesignsMenuOpen = false;
	
	TweenLite.to($('.subMenu'), 0.5, {opacity:0, ease:Power2.easeInOut,
		onComplete:function(){
			$('#designsMenu').css("margin-bottom",$menuHeightOnDesigns - $menuHeight);
			$('.subMenu').remove();
			if(nextMenu == 'void'){
				TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeight)/2, ease:Power2.easeInOut});
				TweenLite.to($('#designsMenu'), 0.5, {marginBottom:8, ease:Power2.easeInOut});
				TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#designsMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#biosMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
			}
			if(nextMenu == 'projects') openProjectsMenu('designs');
			if(nextMenu == 'sketches') openSketchesMenu('designs');
		}
	});

} // end function

function closeSketchesMenu(nextMenu){
	
	$isSketchesMenuOpen = false;
	
	TweenLite.to($('.subMenu'), 0.5, {opacity:0, ease:Power2.easeInOut,
		onComplete:function(){
			$('#sketchesMenu').css("margin-bottom",$menuHeightOnSketches - $menuHeight);
			$('.subMenu').remove();
			if(nextMenu == 'void'){
				TweenLite.to($('#menu'), 0.5, {marginTop:($(window).height() - $menuHeight)/2, ease:Power2.easeInOut});
				TweenLite.to($('#sketchesMenu'), 0.5, {marginBottom:15, ease:Power2.easeInOut});
				TweenLite.to($('#bioMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#sketchesMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
				TweenLite.to($('#designsMenu'), 0.5, {marginTop:0, ease:Power2.easeInOut});
			}
			if(nextMenu == 'projects') openProjectsMenu('sketches');
			if(nextMenu == 'designs') openDesignsMenu('sketches');
		}
	});

}
